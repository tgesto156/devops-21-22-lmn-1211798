###DevOps Report - GitHub The Basics

<p>The main purpose for this report is to get and introduction to GitHub and learn its basic commands 
and functionalities for hosting software for development online and making it easier to work with via
version control.</p>
<p>The first step after installing Git is to setup your account using the terminal. For that you use
the command <strong>git config --global user.email email@email.com</strong> and replacing the email field
is to be replaced with the email you setup your Git account with.</p> 

<p>To start working you should head into the Bitbucket website and setup a new empty repository by 
pressing the plug sign on the left sidebar of the website and choosing the create new repository option.
The name of the repository should be <strong>devops-21-22-LMN-1211798</strong> where the LMN is the initials
of the teacher lecturing the class and the final number is your student number.</p>
<p>There should be issues created on the Bitbucket repository for all the tasks that need to be completed in
in order to finish the tutorial.</p>
<p>Next we need to initialize our repository and to do that you must use the command <strong>git init</strong>
to start your repository</p>
<p>Then you'll need to download the necessary files from the Git repository using the command
<strong>git clone https://github.com/spring-guides/tut-react-and-spring-data-rest</strong>. The files should
now be downloaded to the terminal's current working directory. The files should then be added onto a specific
folder for the class which you should name <strong>CA1</strong>.</p>
<p>You'll then need to commit and push the files you just downloaded onto your repository. To do that you must
first start a version control of those files by using the <strong>git add</strong> command followed by 
<strong>git commit -m ’initial project version’</strong> and then sending the files to the repository
by using the <strong>git push origin master</strong> command. Now the files you downloaded should show
up in your Bitbucket repository.</p>
<p>Commits can be marked with tags created by you. To do that you must use the command <strong>git tag tag_name
</strong> where you will replace the tag_name field with <strong>V1.1.0</strong> for this version. We can use
<strong>git tag</strong> to see the list of created tasks and push them into the repository with 
<strong>git push origin v1.1.0</strong></p>
<p>You should then do some local changes to the code of the application in your repository so that it includes
a new field and stores information about the number of years that the employee is in the company. You should also
also test those changes with unit testing so that you can verify it only accepts integers.</p>
<p>Once the changes are complete you should create a new tag using the command <strong>git tag v1.2.0
</strong>. You should then use the <strong>git add .</strong> command to send the new version to the staging
area. You can check the files you've changed with the <strong>git status</strong> command where any changed
files should show up with green letters. After adding the files to the staging area you should commit them with
<strong>git commit -m 'message'</strong> where you can type any message you want between quotes.
To finalize you should push the new files into your repository with <strong>git push origin v1.2.0</strong></p>
<p>After completing this first part you should mark your repository with the tag <strong>ca1-part1</strong>.</p>
<p>The next step of this tutorial is creating branches so that you can work on your application for new features
without compromising the stable version we have. First you should use the <strong>git branch email-field</strong>
command where the email-field is the name of your newly created branch. For now this branch is a copy of your
origin branch that we will use to add an email field to the Employee class as well as unit testing for the new
field. Now you'll need to change onto the new branch, for that you need to use <strong>git checkout email-field
</strong> and you can use <strong>git branch</strong> to see the branch you're currently working on, it should
show up with an * before the name on the list and green letters.</p>
<p>After completing the changes to your application and debugging it you should create a new tag with 
<strong>git tag 1.3.0</strong>, followed by <strong>git add .</strong> and <strong>git commit -m 'Added
email field to Employee class'</strong>. Next you'll be going back to your master branch with <strong>
git checkout master</strong> and merging the changes your made into it with <strong>git merge email-field</strong>.
Now you only need to push and tag your master branch into your repository with <strong>git push origin v1.3.0
</strong>.</p>
<p>Let's create a bug fixing branch with <strong>git branch fix-invalid-email</strong>, change into it with
<strong>git checkout fix-invalid-email</strong> and create a verification method on your application so that
email fields are only valid when they have an <strong>'@'</strong> on them as well as unit tests for your
. After you have completed this you can add the new files to the staging area with <strong>git add .</strong>
followed by <strong>git commit -m 'Added validations for email field in Employee class'</strong>. You should
then change back to the master branch with <strong>git checkout master</strong>, create a new tag with
<strong>git tag v1.3.1</strong> and push the changes with <strong>git push origin v1.3.1</strong>. After
completing all this you should mark your repository with <strong>git tag ca1-part2</strong>.</p>

###DevOps Report - Mercurial - Alternative to GitHub

###Repository access with: https://isep2.codebasehq.com/projects/devops-21-22-lmn-1211798-mercurial/repositories/devops-21-22-lmn-1211798-mercurial/tree/tip

<p>As an alternative to GitHub you can use the software <strong>Mercurial</strong> which can be downloaded from
the website <strong>https://www.mercurial-scm.org/</strong></p>
<p>Next you'll need to setup the program for it to have an email to associate with our commits. For that you need
to head to the <strong>%USERPROFILE%\</strong> folder on windows and create a new file with the name <strong>Mercurial.ini
</strong> and inside the file write the following:</p>
<p></p>
<p><strong>[ui]
username = John Doe 'john@example.com'</strong></p>
<p></p>
<p>You can then add the first version of the project you worked before to a new folder and push it to the 
Mercurial repository. In order to push you must first convert the folder into a mercurial repository using
the <strong>hg init</strong> command followed by <strong>hg add .</strong> to add all the files to the 
staging area, follow it with <strong>hg commit -m 'First version'</strong>. Now to store your repository
you can head over to <strong>https://www.codebasehq.com/</strong> and setup an account as well as a repository
for mercurial. We can then tag the version by creating a tag with <strong>hg tag v1.1.0</strong> and then tagging
the commit with <strong>hg tag -f v1.1.0 'commit hash'</strong> and typing the commit hash.</p>
<p>Then you'll need to push your files onto the repository by using <strong>hg push 'repository
url'</strong> and change the repository url field onto the https url of the repository you just created. The
terminal will ask for your account information and after typing your email and password your files will be
pushed to the repository.</p>
<p>Next you should do changes to your application like you did before to include the jobYears field for the 
Employee class and unit tests and after debugging you should push the new version  <strong>hg add .</strong>
, followed by <strong>hg commit -m 'Added job years to employee class'</strong> and finally <strong>hg push 
'repository url'</strong>. You should also tag it with <strong>hg tag -f v1.2.0 'commit hash'</strong>.</p>
<p>Now you'll be creating branches with mercurial. Create a new branch called email-field with the command
<strong>hg branch email-field</strong> and then push it onto the repository. Then use <strong>git branches
</strong> to see if the new branch was added to the repository. After that work on your software and add
the email field for the employee class and unit testing for it. Once completed and debugged use the command
<strong>hg update default</strong> and <strong>hg merge email-field</strong>to merge the changes on the 
current email-field branch onto the default branch and commit and push it onto the repository like before.
Tag your new commit with v1.3.0.</p>
<p>You'll now use branches to create a hotfix for your email field, first you'll create a new branch with
<strong>hg branch fix-invalid-email</strong> and commit and push it into the repository with <strong>hg commit
-m 'Added new branch fix-invalid-email'</strong> and <strong>hg push 'repository url'</strong>.</p>
<p>After creating the branch you'll change your software so that it verifies if upon creating a Employee
the email is valid by checking if it has a '@' on the String received and create tests to verify that.</p>
<p>After completing the changes go back to the default branch with <strong>hg update default</strong> and 
merge the changes with <strong>hg merge fix-invalid-email</strong>. After merging both versions use the 
<strong>hg add .</strong>, <strong>hg commit -m 'added email validation methods'</strong> and <strong>hg push 
'repository url'</strong> commands to push the hotfix to the repository. After all this is done tag the
repository by creating a new tag with <strong>hg tag v1.3.1</strong> and <strong>hg tag -f v1.3.1 'commit 
hash'</strong>.</p>
