package Test;

import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void createEmployeeSuccess() {
        Employee employee = new Employee("Tiago", "Gesto", "Switch", "Student", 29, 1, "tiagogesto156@gmail.com");
        assertNotNull(employee);
    }

    @Test
    void createTwoIdenticalEmployees() {
        Employee employee1 = new Employee("Tiago", "Gesto", "Switch", "Student", 29, 1, "tiagogesto156@gmail.com");
        Employee employee2 = new Employee("Tiago", "Gesto", "Switch", "Student", 29, 1, "tiagogesto156@gmail.com");
        assertEquals(employee1, employee2);
        assertEquals(employee1.hashCode(), employee2.hashCode());
    }

    @Test
    void createEmployeeNullName() {
        String expected = "First Name is Invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee(null, "Gesto", "Switch", "Student", 29, 1, "tiagogesto156@gmail.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void createEmployeeEmptyName() {
        String expected = "First Name is Invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("", "Gesto", "Switch", "Student", 29, 1, "tiagogesto156@gmail.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void createEmployeeEmptyEmail() {
        String expected = "Email is Invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("Tiago", "Gesto", "Switch", "Student", 29, 1, "");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void createEmployeeEmptyEmailWithSpaces() {
        String expected = "Email is Invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("Tiago", "Gesto", "Switch",
                    "Student", 29, 1, "     ");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void createEmployeeNullEmail() {
        String expected = "Email is Invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("Tiago", "Gesto", "Switch", "Student", 29, 1, null);
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void createEmployeeInvalidEmail() {
        String expected = "Email is Invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("Tiago", "Gesto",
                    "Switch", "Student", 29, 1, "tiagogesto156gmail.com");
        });
        assertEquals(expected, exception.getMessage());
    }


}