###DevOps Report - Build Tools with Gradle - Part 1

<p>The purpose of this lesson was to understand how to work with an alternative
to Maven called Gradle.</p>
<p>Gradle is a free to use build automation tool that supports multiple programming
languages. This tool can automate the process of creating applications by compiling,
linking and packaging the code. Unlike Maven, Gradle does not use the XML build file
but instead utilizes it's own programming language called Groovy.</p>
<p>The first thing to do to start the class is to download the application from 
<strong>https://bitbucket.org/luisnogueira/gradle_basic_demo/</strong>, placing it
inside a folder named CA2 and then another folder named Part1 and adding it to the
personal repository.</p>
<p>After reading the readme.md it is requested that we create a .jar file for the
application by using:</p>

    % ./gradlew build

<p>The application can be experimented by using this commands in two different
terminals:</p>

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>
    % ./gradlew runClient

<p>In the server port it is expected to use the localhost set for this application which
is <strong>59001</strong></p>
<p>To avoid having to use two terminals a task can be created in the build.gradle file, 
using the groovy language that can execute the server. This task should be of a <strong>
JavaExec</strong> type and depend on the <strong>basic_demo.ChatServerApp</strong> class 
and should be given the server port as an argument.</p>

    task runServer(type: JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client server"

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'

    args '59001'}

<p>It was then necessary to add a unit test for the application and make the necessary changes
to the gradle script for the tests to be executed. For the tests to run we'll need to add the
the following line to the dependencies in the build.gradle file:</p>

    testImplementation('junit:junit:4.12')

<p>After the dependencies are added the tests can be executed by heading to the terminal and using
the command:</p>

    ./gradlew test

<p>Next we'll attempt to create a task in the build.gradle file that allows us to backup the content
from the <strong>src</strong> folder into a new folder named <strong>backup</strong>. For that we'll 
be required to create a new task of the <strong>copy</strong> type. Below we can see the code used for
this class:</p>

    task backupFiles(type: Copy) {
    from('src')
    into('backup')
    }

<p>The from section of the code is the folder name we want to copy and the into is the name of the folder
where our copy will be stored. We can then call this task by heading to the terminal and using the command:</p>

    ./gradlew backupFiles

<p>The last task for this class was to create another copy of the <strong>scr</strong> folder but this time the
backup should be stored in a <strong>zip</strong> compressed file. We'll need to build a new task where the 
type is <strong>zip</strong></p>. This is an example of the code used:

    task backupToZip(type: Zip) {
    archiveFileName = "backup.zip"
    destinationDirectory = file("backupzip")
    from "src"
    }

<p>The <strong>archiveFileName</strong> variable is the name given to the new file and the <strong>
destinationDirectory</strong> is the folder where our file we'll be saved. Like before the from section
refers to the path that we'll be saved on our backup. The task can be then used from the terminal with
the command:</p>
    
    ./gradlew backupToZip

<p>Once all the steps are complete we should commit our application onto the repository and mark it with
the tag <strong>ca2-part1</strong></p>
<p>By the end of this class it is expected that we can better understand how gradle and the groovy language
work and how to make simple tasks in the build.gradle file as well as add dependencies to it. We can see
just how powerful the gradle automatic build tool can be for future usages when building applications in a
different set of programing languages.</p>

###DevOps Report - Build Tools with Gradle - Part 2

<p>For the second part of the work we'll be converting an application that uses Maven into one that utilizes
Gradle. To start we must create a new branch for the work we'll be doing and name it <strong>tut-basic-gradle
</strong>.</p>
After the branch is created we'll need to use <strong>Spring Initializr</strong>. This web-based tool is a quick
start generator for Spring Boot projects. We can access this tool by the following <strong>https://start.spring.io/
</strong> and after being on the main page of the website we'll be choosing the settings for our project according
to the ones provided in the class assignment tutorial.

    Project: Maven
    Language: Java
    Spring Boot: 2.6.6

    Project Metadata
    Group: com.greglturnquist
    Artifact: react-and-spring-data-rest-basic
    Name: react-and-spring-data-rest-basic
    Description: An SPA with ReactJS in the frontend and Sprint Data REST in the backend
    Package Name: com.greglturnquist.payroll
    Packaging: Jar
    Java: 11

    Dependencies:
    Rest Repository
    Thymeleaf
    Spring Data JPA
    H2 Database

<p>After pressing generate Spring Initializr will provide us with a zip file that will be used for our project.
We'll be extracting that zip file onto our CA2 folder and create a new folder named Part2. This files will 
represent an empty spring application to which we'll be adding functionalities.</p>
To add our functionalities we'll be deleting the <strong>scr</strong> folder and replace it with the folder with
the same name from the project we used in the first assignment. This project can be accessed at: <strong>
https://github.com/spring-guides/tut-react-and-spring-data-rest</strong>. We'll also need to copy the <strong>
webpack.config.js</strong> and <strong>package.json</strong> and delete the folder at <strong>
r src/main/resources/static/built/</strong> since this last folder needs to be generated automatically from the 
javascript by the webpack tool.
<p>We can now run our application by using the command:</p>

    ./gradlew bootRun
<p>But nothing will show up at <strong>http://localhost:8080</strong> since Gradle has no plugin to address
frontend code. In order for the application to handle frontend we'll need to head to the build.gradle file
and add the information for our plugin in the code. We'll need to write the following line in the file:</p>

    id "org.siouan.frontend-jdk11" version "6.0.0"

<p>Make sure to select the correct <strong>jdk</strong> version used in the application to avoid conflicts. We then need to
add the code below to configure our plugin so that the frontend will show up correctly.</p>

    frontend {
    nodeVersion = "14.17.3"
    assembleScript = "run build"
    cleanScript = "run clean"
    checkScript = "run check"
    }

<p>We'll also need to update the scripts section of the <strong>package.json</strong>file to configure 
the webpack. For that we can add the following code to the file:</p>

    "scripts": {
    "webpack": "webpack",
    "build": "npm run webpack",
    "check": "echo Checking frontend",
    "clean": "echo Cleaning frontend",
    "lint": "echo Linting frontend",
    "test": "echo Testing frontend"
    },


<p>After everything is set up and configured we can build our application by using the command:</p>

    ./gradlew build

<p>If everything was properly setup we should get similar text in our terminal:</p>

    $ ./gradlew build
    BUILD SUCCESSFUL in 1s
    4 actionable tasks: 4 up-to-date

<p>After using the command:</p>
    
    ./gradlew bootRun

<p>We'll be able to see the frontend of our code in the <strong>http://localhost:8080</strong> url.</p>

<p>For the next part of the work we'll be creating a task in the build.gradle file to copy our files into
a folder named <strong>dist</strong> in the root project. The task should be of the Copy type and we need 
to specify that the jar files are located in the <strong>build/libs</strong> directory.The code below is 
an example of how to achieve this:</p>

    task copyJar(type: Copy) {
	from "build/libs"
	into "dist"
    }

<p>Running the code:</p>

    ./gradlew copyJar

<p>Will create a new folder in the root of the Part2 folder named dist that will contain the project's jar files.
</p>
<p>To end our modifications we need to create another task in the build.gradle file that can delete all the files
in the <strong>src/resources/main/static/built/</strong> and this time the task should be executed before the already
available task <strong>clean</strong>. This task will be of the Delete type and we'll need to add the directory we
want to delete. Along with the directory we'll need to instruct the task to be done before the <strong>clean
</strong> task. For that to happen we add the line <strong>clean.doFirst {task name}</strong> The code below 
is an example on how to do it:</p>

    task deleteWebpackFiles(type: Delete) {
	delete 'src/main/resources/static/built/'
    }
    clean.doFirst {deleteWebpackFiles}

<p>After completing all the tasks we can head back to the main branch with the git commands we learned last
class and merge the newly created branch with the master one. We can then commit all the changes to the repository
and tag it with <strong>ca2-part2</strong></p>