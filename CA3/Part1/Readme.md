###DevOps Report - Virtualization - Part 1

<b>Virtualization</b> is an emulated hardware environment that can be created using specific Software like 
<b>VMware</b> or <b>VirtualBox</b>. Virtualization can be quite useful for its capability of generating multiple
computer environment <b>Virtual Machines</b> utilizing only the hardware of a single computer. 
<p>There are two different types of Virtualization:</p>

    Hardware-Level Virtualization

    Hosted Virtualization

<p>For Hardware-Level virtualization the virtual machine creation software runs at a hardware level and for
Hosted virtualization it runs at a Operative System level.</p>
<p>Virtualization has a different number of advantages like for example the ability of reproducing the computational
environment multiple times, running the same virtual machine across different OS or sharing the the environment with
different people.</p>


<p>For this lecture we will be using Virtual Box as the hypervisor that runs or Virtual Machine. The software can
be downloaded from the following URL:</p>
 
- https://www.virtualbox.org/


<p>After we will need to download the <b>ISO</b> file that will be used to start our Virtual Machine. This ISO file 
has a minimalistic version of the Ubuntu OS without a graphic interface. Meaning any action done has to be made
via command line. The ISO can be downloaded from the URL below:</p>

-    https://help.ubuntu.com/community/Installation/MinimalCD

<p>When we have both the Virtual Box software and the ISO file ready we can open Virtual Box and press the
<b>New</b> button on the top right. After we can select a name for our Virtual Machine and the folder
where the files will be located, as well as selecting Linux 64-Bit version as the OS we will be making a 
virtualization of.</p>
<p>The next step will ask us to select how much RAM we want to dedicate towards our VM, for this example we will
be selection 2048MB of RAM. One must take into account that we cannot virtualize a bigger amount of RAM than
the one that the host has in real Hardware.</p>
<p>Next we will be asked to select how much Disk Space we want to allocate for the VM to use. We can select the
<b>Create a virtual disk now</b> option from the menu that will allocate the chosen amount from our real Disk 
for the VM. We then choose the <b>VDI</b> disk file type and <b>Dynamically Allocated</b> so that the selected
disk space will not be used all at once but instead will grow as we create files in our VM.</p>

<p>Once everything is configured in our VM we will need to select the bootable ISO file that will be used to 
start our VM. We can head to the VM we just created and choose settings and under the <b>Storage</b> tab we 
choose the <b>Controller: IDE</b> field and choose the ISO file we downloaded previously.</p>
<p>This should conclude the initial preparation to run our VM so we can head to the starting page of Virtual Box
and click the <b>Start</b> button to run it. A window with the OS should pop-up and we can proceed with the normal
installation of the OS following the presented steps.</p>

<p>When the OS is fully installed we should have an instance of Ubuntu terminal running that allows us to use it
just like a real computer.</p>

<p>The next thing we want to do is to make sure we can connect between our VM and our real computer though either
SSH or FTP. For that we will need to configure a second network adapter in our VM that allows us to establish a 
connection with the host. In order to do this we need to head over to the Virtual Box settings and select 
<b>Host Network Manager</b> and just create a new Network adapter. After the adapter is created we can stop the
VM that's currently running and select the newly created adapter in the VM settings as a <b>Host Only Adapter
</b>.</p>

<p>When everything is set we can head back into the VM terminal and type the commands:</p>

    % sudo apt update
    % sudo apt-install net-tools

<p>We'll then need to update the file located in: </p>

    % sudo nano /etc/netplan/01-netcfg.yaml

<p>So that it looks like this:</p>

    network:
        version: 2
        renderer: networkd
        ethernets:
            enp0s3:
                dhcp4: yes
            enp0s8:
                addresses:
                    - 192.168.56.5/24

<p>And apply the changes with the command:</p>

    % sudo netplan apply

<p>This should configure our network adapter and now we can install <b>openssh-server</b> to establish a 
SSH connection between our host and the VM, for that we use the command:</p>

    % sudo apt install openssh-server
    
<p>And we need to enable the password authentication for it using:</p>

    % sudo nano /etc/ssh/sshd_config

<p>We uncomment the line PasswordAuthentication yes</p>

    % sudo service ssh restart

<p>And we can also install <b>FTP - file transfer protocol</b> with the command:</p>

    % sudo apt install vsftpd

<p>Then we enable write access by using the commands:</p>

    % sudo nano /etc/vsftpd.conf

<p>We uncomment the line write_enable=YES and then restart it with:</p>

    % sudo service vsftpd restart

<p>It is now possible to establish a SSH connection between our host and the VM, we can open a terminal in the host
and type the command:</p>

    % ssh username@192.168.56.5
    
<p>Being the username the name of our VM and the IP address the one from the network adapter we configured
previously.</p>
<p>We also created an FTP server in our VM so we are able to transfer files using different ftp software like
the one we can find in the URL below:</p>

-    https://filezilla-project.org

<p>Our goal was to execute our Spring application on the VM, for that we first need to install both Git and Java
using the commands:</p>

    % sudo apt install git
    % sudo apt install openjdk-8-jdk-headless

<p>Once everything is installed correctly we can make our Application run by getting a clone of our Repository
that was used in a previous classes to our VM, we can get it using:</p>

    % git clone https://tgesto156@bitbucket.org/tgesto156/devops-21-22-lmn-1211798.git

<p>Then we can enter the /basic folder and in order to run our Spring application we just need to use the commands:
</p>
    
    % cd CA1\tut-react-and-spring-data-rest
    % ./mvnw spring-boot:run

<p>Once it finishes loading we should be able to access the application we just started in the VM from our
host computer. For that you enter your host browser and enter the URL:</p>

-    http://192.168.56.5:8080/

<p>In order to run our gradle-basic-demo we need to install gradle on the VM, to do so we'll need to use the
following commands:</p>

    % cd /tmp
    % wget https://downloads.gradle-dn.com/distributions/gradle-6.4.1-bin.zip
    % sudo unzip -d /opt/gradle gradle-6.4.1-bin.zip

<p>To run our gradle application from the CA2 we'll need to give execution permission to our <b>gradlew</b>
file using the command:</p>

    % chmod +x gradlew

<p>And now we can run our server in our VM instead of our host. Keep in mind since the VM has no graphic
interface the App can't be run on it but the server can by using the command.</p>

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>

<p>We can then connect to it from our host computer by using the command:</p>

    % ./gradlew runClient

<p>As we can see virtualization is extremely useful for running multiple servers on the same Hardware and allows
us to create multiple copies of the same server without the need for much effort.</p>

