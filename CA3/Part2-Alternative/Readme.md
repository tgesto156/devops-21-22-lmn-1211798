######DevOps Report - Vagrant Part2 - Alternative

<p>As an alternative to the virtualization tool <b>Virtual Box</b> we can instead use another tool named
<b>VMware</b>. The following tool can be found in the URL below:</p>

-   https://www.vmware.com/


<p>Both tools can be very effective for the creation of Virtual Machines although there are some differences
between them:</p>

|          |      VirtualBox      |  VMware |
|----------|:-------------:|------:|
| Virtualization |  Hardware + Software | Hardware |
| Host OS |    Linux, Windows, Solaris, macOS, FreeBSD   |   Linux, Windows |
| Guest OS | Linux, Windows, Solaris, FreeBSD, macOS |  Linux, Windows, Solaris, FreeBSD |
| Disk |  VDI, VMDK, VHD | VMDK |
| Snapshots |  Yes | No |
| Max Video Memory |  128mb | 2gb |
| Hypervisor Type |  2| 2 |

<p>Both VMware and VirtualBox can be extremely versatile for virtualization, although in the case of Virtual
Box we have the plus side of a more user friendly interface and the support for both Hardware and Software
virtualization while the VMware excels in having a higher Video Memory limit which allows for virtualization
of things that require 3D graphics.</p>

<p>To start our alternative we first need to install VMware which can be downloaded from the URL below:</p>

    % https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html

<p>After installing VMware we can now create a new folder named <b>Part2-Alternative</b> and copy the application and
the <b>Vagrantfile</b> from the <b>Part2</b> folder we used previously.</p>
<p>Next we'll need to do some modifications to our Vagrantfile so that it runs on VMware and not Virtual Box.
For that we'll need to change the <b>vm.provider</b> field to "vmware_desktop" as well as change the path for
our application to the correct folder in the provision file.</p>

-         cd devops-21-22-lmn-1211798/CA3/Part2-Alternative/react-and-sprint-data-rest-basic

<p>It is also necessary to make changes to the IPs selected for our VMs since VMware assigns a certain IP to them.
In order for everything to work we'll need to change the previous IPs we had in our Vagrangfile to the ones below:</p>

- 192.168.42.11 - Database
- 192.168.42.10 - Web Server

<p>We'll need to change the type of box used in the Vagrantfile since the one we used for our previous example is
not compatible with VMware. For that to happen we need to change both the webserver and database box as below:</p>

- From: envimation/ubuntu-xenial
- To: hashicorp/bionic64

<p>The final change will be done to our <b>application.properties</b> file from the application where we'll need
to change to the correct IP we selected earlier so that we get the following line:</p>

-   spring.datasource.url=jdbc:h2:tcp://192.168.42.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE


<p>We'll also need to install a plugin for vagrant to work with VMware with the command:</p>

    % vagrant plugin install vagrant-vmware-desktop

<p>Now everything should be ready for us to start our VMs with:</p>

    % vagrant up

<p>Once everything is up and running we should be able to access the webserver and the database from our browser
by using the following URLs:</p>

- http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/
- http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console

<p>From implementing the alternative we can understand that there are multiples ways to create VMs using Vagrant, 
yet we can notice that VirtualBox can be more user friendly when it comes to the UI and the way we interact with it.
Also we should know that the default provider for Vagrant is VirtualBox which can make it much more simple to work with
it in comparison with VMware.</p>