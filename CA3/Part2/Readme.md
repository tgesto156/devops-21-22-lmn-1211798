######DevOps Report - Vagrant Part1

<p>For this class assignment we will be creating VMs using Vagrant. Vagrant is a tool that allows us to
build and manage multiple VMs in an automated way. It does so by only requiring us to make changes to a 
simple configuration file and then automates the whole process of creating the VM making it very easy
to manage multiple VMs or sharing them with colleagues.</p>

<p>The first thing we're required to do is install Vagrant from the URL below:</p>

-   www.vagrantup.com

<p>We can then verify the version we're using by using the command below:</p>

    % vagrant - v

<p>We'll then need to create a new folder named vagrant-project-1 and inside it type the following command
in order to generate a new Vagrant configuration file:</p>

    % vagrant init envimation/ubuntu-xenial

<p>To start our VM we can simple use the command:</p>

    % vagrant up

<p>And then to establish an SSH connection we'll just need to use the command:</p>

    % vagrant init envimation/ubuntu-xenial

<p>We can replace the box name above with a different one from the list found in this URL:</p>

- https://app.vagrantup.com/boxes/search

<p>We can then make changed to our Vagrant provision file in order to suit our needs. We can open the <b>Vagrantfile
</b> and make the following changes.</p>

- remove the # from the line related to the ports and change the host port from 8080 to 8010
- remove the # from the line related to the private-network and change the ip from 192.168.33.10 to 192.168.56.5
- remove the # from the line related to the sync-folders

<p>Finally once all changes are made we can simple reload the provision file with the command:</p>

    % vagrant reload --provision

<p>If everything went correctly we can head to the browser on our host machine and see if the server is running
by entering the URL:</p>

-   localhost:8010

######DevOps Report - Vagrant Part2

<p>For this second part we will want to put 2 VMs running at the same time in our host computer. One will have
a Database for our application and the other will have a Webserver for the same application.</p>
<p>The first thing will be to create a new folder in our repository and clone the contents from the repository
below onto it:</p>

- react-and-sprint-data-rest-basic

<p>Now we'll need to change the provision file so that it uses the application we previously modified in our
repository. For that we need to add a few instructions for the configuration file to execute upon starting the VM</p>
<p>The first thing we'll need to change is the command to download the repository, for that we'll need to 
replace the existing clone command for a new one and a command to change into the correct path. For this
to work we'll first need to make our repository public so that the VM can access it.</p>

    % git clone https://tgesto156@bitbucket.org/tgesto156/devops-21-22-lmn-1211798.git
    % cd devops-21-22-lmn-1211798/CA3/Part2/react-and-sprint-data-rest-basic
    % chmod u+x gradlew
    % ./gradlew clean build

<p>After we have our provision file ready we need to do some modifications to the files inside the repository
we just cloned. To do so we need to head over to:</p>

- https://bitbucket.org/atb/tut-basic-gradle

<p>Then we need to follow the commits done by the teacher and apply the changes on our own files in the repository
we cloned. When all the files have been modified in accordance with the commits done by the teacher we are ready
to start our VM. For that we head to the folder where the provision file is placed and use the command:</p>

    % vagrant up

<p>If everything was done correctly both VMs should be operational and should show up as running in the interface
of Virtual Box. After everything is running we can see the application by accessing both this URLs:</p>

- http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/
- http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console

<p>The first URL will show us the web application and the second one will give us access to the database.</p>
<p>In conclusion we can see how efficient Vagrant is as a tool to create VMs with all the specifications 
we need and share them with colleagues. The only thing required is a configuration file and an understanding
of how to make changes to the configuration file in order for the VM to work as intended.</p>

