######DevOps Report - Containers with Docker Part 2

<p>For this assignment we will be doing the same as on the previous one but instead of building the application
directly on the Docker Image, we will be copying a pre-built .jar file for the application onto the Docker image
and running it from there.</p>
<p>So to create our .jar file we need to head to the <b>CA4/Part1/gradle_basic_demo</b> directory and run the 
following command:</p>

    % ./gradlew clean build

<p>This will create a clean build for our app and give us a new .jar file that we will be copying to the image.
The file should be in the <b>CA4/Part1/gradle_basic_demo/build/libs/basic_demo-0.1.0.jar .</b> directory and we
can now copy it to our <b>CA4/Part2</b> folder. Now we need to create a new Dockerfile in order to build or
Docker Image which will execute the jar file which contains our chat server application. The file should
contain a command that allows us to copy the file onto our image like one below along with the commands we used
previously. Doing this will allow us to skip installing git and cloning the application from the repository
to the Docker Image.</p>

- COPY ./basic_demo-0.1.0.jar .

<p>Now we only need to use the Docker to build our image with the command:</p>

    % docker build -t chat-server-image-jar .


<p>Once our image is done we can just run it with:</p>

    % docker run --name jarcontainer -p 59001:59001 -d chat-server-image-jar

<p>And if we want to try it out we just need to run the command from our application folder:</p>

    % ./gradlew runClient

<p>After everything is done we can just tag the image and push it onto the Docker Hub:</p>

    % docker tag chat-server-image-jar tgesto156/devops-21-22-lmn-1211798:ca4-part2
    % docker push tgesto156/devops-21-22-lmn-1211798:ca4-part2

<p>This should conclude the assignment for CA4 Part2. We can see just how much simpler and faster it is if we 
just copy the jar file onto the image eliminating the need to install git and cloning the repository when the 
Docker first builds the image.</p>

<p>The repository with the Docker images can be found at:</p>

- https://hub.docker.com/repository/docker/tgesto156/devops-21-22-lmn-1211798