######DevOps Report - Containers with Docker Part 1

<p>Building a container is an efficient way to package an app or a software and all it's dependencies and
configuration files into a container image. They are extremely useful since they isolate applications
from each other while on the same OS. This method allows for the deployment of the same application
on different OS without the need to do much modification.</p>

<p>The software we'll be using for containerization is called <b>Docker</b> and can be downloaded from
the URL below:</p>

- https://www.docker.com/get-started/

<p>This software is open source and allows us to create and share docker files that can be used for 
nearly everything. one of the advantages of using Docker is that it allows us to upload our Docker
images on the cloud or utilize other images already there that have what the specifications we 
need for our project. All those images can be seen in the following URL:</p>

- https://hub.docker.com/

<p>For the work we will be doing fo this assignment we can start by creating a new folder in our 
repository named <b>CA4/Part1</b>. The aim of this will be to create a Docker image that can run our chat
application from CA2 that can be found bellow:</p>

- https://bitbucket.org/luisnogueira/gradle_basic_demo/src

<p>After having docker installed on our computer and changing into the newly created folder directory we only 
need to create a Dockerfile with the correct instructions for our image to work. We can first clone the chat
application on our CA4/Part1 folder to have a clean version of it and commit it to the repository. This will 
be important since we'll be cloning the application from our repository to the docker image via git.</p>

<p>When we finish with the application commit we can start building our file which we will need to name <b>Dockerfile
</b>. When the Docker software sees files under this name it will use them to create the image in the folder it is
located. The Dockerfile created should look like the one found in the same folder in the repository.</p>

<p>In the Dockerfile different commands will have different jobs when building the image:</p>

- <b>FROM</b>: Should be the first command on the file and will tell us what OS the image will use, in our case we will choose <b>ubuntu:18.04</b>
- <b>RUN</b>: Tells the image what commands to run open creation and will only execute them one time. The second time we start 
the container Docker will ignore them
- <b>WORKDIR</b>: Changes the current directory to the one we specify after the command
- <b>CMD</b>: These commands will always run upon starting the image
- <b>EXPOSE</b>: Informs Docker that the container listens on the specified network ports at runtime

<p>Now the only thing left to do is to create the Docker image, for that we can use the command:</p>

    % docker build -t chat-server-image .

<p>We can see the newly created image with:</p>

    % docker images

<p>This will create an image based on our Dockerfile that we can then run with:</p>

    % docker run --name my-container -p 59001:59001 -d chat-server-image

<p>Our image can now be seen as running if we use the command:</p>

    % docker ps

<p>To test what we just did we can start our application and see if the server is indeed running. For that
we can use the command below inside our application folder:</p>

    % ./gradlew runClient

<p>The final step is to push our image onto DockerHub. First we will need to head to the website and sign in and
create a new repository and then we need to tag and push our images into it. For that we can use the following commands
</p>

    % docker tag chat-server-image tgesto156/devops-21-22-lmn-1211798:ca4-part1
    % docker push tgesto156/devops-21-22-lmn-1211798:ca4-part1

<p>We should now see a version of our image in the repository. This shows just how easy it is to work with Docker
and how we can share applications in an easy and clean way that is both user friendly and developer friendly.</p>
<p>The repository with the Docker images can be found at:</p>

- https://hub.docker.com/repository/docker/tgesto156/devops-21-22-lmn-1211798


