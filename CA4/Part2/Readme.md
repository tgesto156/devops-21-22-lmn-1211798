######DevOps Report - Containers with Docker Part 2

<p>For this report we will be building a composed Docker file that will allow us to integrate multiple Docker
Images with different servers or applications into the same file. First we will need to create a new directory
for our assignment under the name <b>CA4/Part2</b> and change directories into it. After that is done we will
be using docker compose to 2 containers like the work we did with Vagrant on CA3. First thing we can do is commit
a clean version of the tut-basic-gradle we used previously onto our directory.</p>
<p>We can then head into the <b>tut-basic-gradle\src\main\resources</b> directory and open the <b>application.properties
</b> file located inside. We will be changing the IP on the fifth line onto the one below:</p>

- tcp://192.168.33.11:9092/

<p>That will complete the changes needed on the application and we can now focus on building the Dockerfiles
for both services and the docker-compose file that will generate both images.</p>
<p>First we'll need to create a folder named <b>CA4\Part2\docker-compose-spring-tut-demo</b> and change into 
the directory. Then we will be creating 2 distinct folders for our <b>web</b> and <b>db</b> where we will
be placing a Dockerfile for each service. The code for those files can be seen in the respective folders in
the repository.</p>
<p>The last thing we need to do is create a docker-compose file that will use both Dockerfiles at the same time.</p>
<p>The docker-compose file should reference both Dockerfiles and the networks they will be functioning on. The
code for the <b>docker-compose.yaml</b> file can be seen below:</p>

```
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data-backup
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24
```

<p>As you can see we change the IP address in the application previously to match the one used in our file above.</p>
<p>The next still will be to run the command to create our composed imaged, use the command below for that:</p>

    % docker-compose build

<p>Once the images are done being build we can just use the next command to start them with Docker:</p>

    % docker-compose up

<p>If everything was done correctly we can head over to the URL below to see our database:</p>

- http://localhost:8080/basic-0.0.1-SNAPSHOT/


![alt text](Capture3.PNG)

<p>We can also use the URL below to manipulate our database:</p>

- http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console/

![alt text](Capture1.PNG)

<p>We can access the database with the connection string:</p>

- jdbc:h2:tcp://192.168.33.11:9092/./jpadb

![alt text](Capture4.PNG)

<p>Last thing we need to do is commit the changes onto the repository and the Docker images into Docker Hub.</p>
<p>This should conclude the assignment and show us just how easy it is to create multiple services simply using
text files from Docker.</p>

