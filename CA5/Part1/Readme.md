######DevOps Report - Jenkins

<p>The goal of this report will be to create a pipeline using the <b>gradle basic demo</b> we used before.
 We use a <b>continuous integration/continuous delivery (CI/CD)</b> pipeline so that the software delivery
process becomes a lot more automated. This allows for an application to be <b>built, tested and deployed</b> every time
a change is made to it.</p>

<p>For this we will be using a software called <b>Jenkins</b> which is capable of doing all this steps before
doing the final build for the application in the current state it is presented. For this task we will be downloading
a Docker Container which already contains Jenkins and running it. To download a container with Jenkins we 
can use the command: </p>

    % docker pull jenkinsci/blueocean

<p>Jenkins is an open source software that allows for an easy way to create a (CI/CD) pipeline that can build,
test and deploy an application from a repository.</p>


<p>We will then create a bridge network called jenkins by using the command:</p>

    % docker network create jenkins

<p>And then we can run the server with the following command that contains all the settings:</p>

    % docker run --name jenkins-docker --rm --detach --privileged --network jenkins --network-alias docker
    --env DOCKER_TLS_CERTDIR=/certs --volume jenkins-docker-certs:/certs/client
    --volume jenkins-data:/var/jenkins_home --publish 2376:2376 docker:dind --storage-driver overlay2

<p>Next we will use the command bellow to allow Jenkins to be accessible from another computer:</p>

    % docker run -u root --rm -d -p 8080:8080 -p 50000:50000 -v $HOME/jenkins:/var/jenkins_home -v 
    /var/run/docker.sock:/var/run/docker.sock jenkinsci/blueocean

<p>When we finish running our docker images our docker desktop should look like this:</p>

![alt text](Capture5.PNG)


<p>Upon running the terminal will show us a Jenkins password that we will need for later so we need to 
save it in a text file. Now we head to the URL below to access the jenkins menu:</p>

    % http://localhost:8080/

<p>Heading to the localhost will show us a Jenkins menu where we will be creating our pipeline. In order
to do so the first thing we need is to create a script named Jenkinsfile with all the stages that the pipeline
will have. The file will be placed inside the basic_gradle_demo folder so that it knows what the application
to create a pipeline for is. This file should contain stages like the <b>Checkout</b> stage where it connects
to the repository we will be using, the <b>Assemble</b> stage where it builds the application using <b>gradle</b>,
the <b>Tests</b> stage where it will run the application tests and see if any of them fail and the <b>Archiving</b>
stage that creates files with the results from our build.</p>
<p>Next we can head onto the Jenkins menu and start a <b>New Item</b> from the sidebar, name it and select the
<b>pipeline</b> option. Further down the settings on <p>Pipeline</p> we select <b>Pipeline script from SCM</b>
and we place on the line below the URL for our git repository. In my case I used:</p>

- https://bitbucket.org/tgesto156/devops-21-22-lmn-1211798/


<p>Further down on the <b>Script Path</b> setting we need to place the directory of our Jenkinsfile inside the
repository. In this case it will be <b>CA5/Part1/gradle_basic_demo/Jenkinsfile</b> and save our settings.
Next we need to select <b>Build Now</b> from the sidebar and wait for the build to finish.</p>

<p>The settings for the Jenkins build should be site like this:</p>

![alt text](Capture4.jpeg)

<p>After hitting build we will see a set of boxes with all the building stages we created on our Jenkinsfile
and a progress bar of the build during those stages. If everything goes correctly we should see all the stages
with a green color like the picture below:</p>

![alt text](Capture3.PNG)

<p>This should conclude the assignment and should make a stable build for our application. Due to a problem
doing the class assignment I was forced to build a fork for the repository and work on that fork. The link
to the repository can be found below:</p>

- https://tgesto156@bitbucket.org/tgesto156/devops-21-22-lmn-1211798-fork.git
