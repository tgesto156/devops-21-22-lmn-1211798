    ######DevOps Report - Jenkins

<p>The goal of this assignment is also to use Jenkins to build a pipeline, much like the previous one, but this time
we will be using the <b>gradle_basic_demo</b> application we used for our CA4 Part2. Like the previous CA
we first need to open our <b>Docker Desktop</b> and start a imagine to run Jenkins on our local host. That can be 
achieved with the commands below that we also used previously.</p>

    % docker pull jenkinsci/blueocean
<p></p>

    % docker network create jenkins

<p></p>

    % docker run --name jenkins-docker --rm --detach --privileged --network jenkins --network-alias docker --env DOCKER_TLS_CERTDIR=/certs --volume jenkins-docker-certs:/certs/client --volume jenkins-data:/var/jenkins_home --publish 2376:2376 docker:dind --storage-driver overlay2

<p></p>

    % docker run -u root --rm -d -p 8080:8080 -p 50000:50000 -v $HOME/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock jenkinsci/blueocean

<p>The difference from this CA to the previous one is that we will be implementing two extra stages to our pipeline.
The first stage we will be adding is will allow us to publish our Javadocs. The code for the stage in the pipeline
will look like the one below:</p>

    stage('Publish Javadoc') {
    steps {
    dir('CA5/Part2/gradle_basic_demo') {
    echo 'Publishing JavaDocs'
    sh './gradlew javadoc'
    publishHTML (target: [
    allowMissing: false,
    keepAll: true,
    reportDir: 'build/docs/javadoc/',
    reportName: 'javadoc',
    reportFiles: 'index.html',
    ])
    }
    }
    }

<p>The gradle plugin has a task that automatically generates a Javadoc <b>./gradle javadoc</b> and then with the
help of the Jenkins <b>HTML publisher</b> plugin we can publish the document. The steps below are options that can
be used for different things.</p>

 - allowMissing - If the report is missing the build will not fail
 - keepAll - When set to true archives all successful build reports and set to false only archives last successful one
 - reportDir - Saves the path to the HTML directory compared to the workspace
 - reportName - Stores the name of the report to display for the build
 - reportFiles - Saves the file(s) to provide links inside the report directory

<p>Documentation for the HTML publisher plugin can be seen here:</p>

 - https://www.jenkins.io/doc/pipeline/steps/htmlpublisher/

<p>Next we will be creating a stage to publish or docker images onto Docker Hub. The first step is to install
a couple plugins on Jenkins, in this case <b>Jenkins Docker</b> and <b>Docker Pipeline Plugin</b>. Then we will
be adding our stage code like the one below:</p>


    stage('Docker Image') {
    steps {
    echo 'Building Docker Image...'
    dir("CA5/Part2/gradle_basic_demo") {
    script {
    dockerImage = docker.build("tgesto156/devops-21-22-lmn-1211798:V0.${env.BUILD_ID}", ".")
    docker.withRegistry("https://registry.hub.docker.com", 'dockerhub'){
    dockerImage.push()
    }
    }
    }
    }
    }

<p>We need to add the URL for our repository in the <b>dockerImage</b> field as well as generate a login token
in the Docker Hub settings page that we will add as credentials on our Jenkins Settings for the pipeline.
The <b>docker.build</b> will build our image for us and the <b>dockerImage.push</b> will push it towards
our repository in Docker Hub.</p>

<p>Like the previous CA once everything is done in our Jenkinsfile we just need to add it in the same folder
as the application we will be building and configure the pipeline settings like the ones below:</p>

![alt text](Capture1.jpeg)

<p>After the settings are correctly selected we can head to the build now section and begin building our pipeline.
If everything was set up correctly we should be able to see something like this:</p>

![alt text](Capture.PNG)

<p>We should also be able to see the image for our build in the Docker Hub. In this case it will be stored in the
URL below with the name <b>V0.12</b> since 12 was the build were the application finally got successfully built.</p>

 - https://hub.docker.com/repository/docker/tgesto156/devops-21-22-lmn-1211798

<p>Due to a problem with the repository I created a fork to store the application and Jenkinsfile. The repository
can be reached in the following URL:</p>

 - https://bitbucket.org/tgesto156/devops-21-22-lmn-1211798-fork/src/master/

<p>After all this steps are done we should have our CA concluded and we can understand how to better operate with Jenkins
in order to generate successful builds for our applications through a pipeline as well as how to further use Jenkins to
push a working image of the application to Docker Hub.</p>